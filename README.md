# Setting Up a RabbitMQ Instance Locally
1. docker run -d --hostname my-rabbit --name some-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management
2. Access UI to http://localhost:15672 using the username guest and password guest.
3. Add user from .env_uat

# Getting Started
1. Copy file /.env_local to /.env
2. Copy file /.env_local to /consume/.env

# For Publish message
1. Run "go mod tidy"
2. Run "go run main.go"

# For Get message
1. Run "cd consume"
2. Run "go run main.go"