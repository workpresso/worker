package main

import (
	"fmt"
	"worker/lib"

	"github.com/joho/godotenv"
	"github.com/streadway/amqp"
)

func init() {
	godotenv.Load()
}

func main() {
	fmt.Println("Go RabbitMQ Tutorial")
	ch := lib.ConnectRabbitMQ()
	defer ch.Close()
	Consumer(ch)
}

func Consumer(ch *amqp.Channel) {
	msgs, _ := ch.Consume(
		"TestQueue",
		"",
		true,
		false,
		false,
		false,
		nil,
	)

	forever := make(chan bool)
	go func() {
		for d := range msgs {
			fmt.Printf("Recieved Message: %s\n", d.Body)
		}
	}()

	fmt.Println("Successfully Connected to our RabbitMQ Instance")
	fmt.Println(" [*] - Waiting for messages")
	<-forever
}
