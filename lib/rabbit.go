package lib

import (
	"fmt"
	"os"

	"github.com/streadway/amqp"
)

func ConnectRabbitMQ() *amqp.Channel {
	rabbitURI := fmt.Sprintf("amqp://%s:%s@localhost:%s",
		os.Getenv("RABBITMQ_USER"),
		os.Getenv("RABBITMQ_PASS"),
		os.Getenv("RABBITMQ_PORT"))
	fmt.Println(rabbitURI)
	conn, err := amqp.Dial(rabbitURI)
	if err != nil {
		fmt.Println("Failed Initializing Broker Connection")
		panic(err)
	}

	// Let's start by opening a channel to our RabbitMQ instance
	// over the connection we have already established
	ch, err := conn.Channel()
	if err != nil {
		fmt.Println(err)
	}

	return ch
}
