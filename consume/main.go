package main

import (
	"fmt"
	"worker/lib"

	"github.com/joho/godotenv"
	"github.com/streadway/amqp"
)

func init() {
	godotenv.Load()
}

func main() {
	fmt.Println("Go RabbitMQ Tutorial")
	ch := lib.ConnectRabbitMQ()
	defer ch.Close()
	PublishMessage(ch)
}

func PublishMessage(ch *amqp.Channel) {
	q, err := ch.QueueDeclare(
		"TestQueue",
		false,
		false,
		false,
		false,
		nil,
	)
	// We can print out the status of our Queue here
	// this will information like the amount of messages on
	// the queue
	fmt.Println(q)
	// Handle any errors if we were unable to create the queue
	if err != nil {
		fmt.Println(err)
	}

	// attempt to publish a message to the queue!
	err = ch.Publish(
		"",
		"TestQueue",
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte("Hello World"),
		},
	)

	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Successfully Published Message to Queue")
}
